<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class uploadController extends CI_Controller {
public function __construct() 
{
parent::__construct();
}
public function file_view()
{
$this->load->view('file_view', array('error' => ' ' ));
}
public function do_upload(){
$config = array(
'upload_path' => "./uploads/",
'allowed_types' => "gif|jpg|jpeg|png|iso|dmg|zip|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|rtf|sxc|sxi|txt|exe|avi|mpeg|mp3|mp4|3gp",
'overwrite' => TRUE,
'max_size' => "2000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
// 'max_height' => "768",
// 'max_width' => "1024"
);
$this->load->library('upload', $config);
if($this->upload->do_upload())
{
$data = array('upload_data' => $this->upload->data());
$upload_data = $this->upload->data();
$filename = $upload_data['file_name'];

$login_data = $this->session->userdata('logged_in');
$user_id = $login_data['user_id'];

$this->load->model('Blog');
$this->Blog->blogadd($filename,$user_id);
$this->load->view('blog',$data);
}
else
{
$error = array('error' => $this->upload->display_errors());
$this->load->view('blog', $error);
}
}
}
?>