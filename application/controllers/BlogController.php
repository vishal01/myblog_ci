<?php


/**
* 
*/
class BlogController extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}



	public function index()
	{
		$this->load->view('headerinner');  
		$this->load->view('blog'); 
		$this->load->view('footer');
	}


	public function Blogview()
	{
		$this->load->model('Blog');
         $data ['query'] = $this->Blog->blogdisplay(); 

		$this->load->view('headerinner');  
		$this->load->view('blog_view', $data); 
		$this->load->view('footer');
	}


	public function contact_info(){
	$config = array();
	$config["base_url"] = base_url() . "index.php/BlogController/contact_info";
	$total_row = $this->Blog->record_count();
	$config["total_rows"] = $total_row;
	$config["per_page"] = 1;
	$config['use_page_numbers'] = TRUE;
	$config['num_links'] = $total_row;
	$config['cur_tag_open'] = '&nbsp;<a class="current">';
	$config['cur_tag_close'] = '</a>';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Previous';

	$this->pagination->initialize($config);
	if($this->uri->segment(3)){
	$page = ($this->uri->segment(3)) ;
	}
	else{
	$page = 1;
	}
	$data["results"] = $this->Blog->fetch_data($config["per_page"], $page);
	$str_links = $this->pagination->create_links();
	$data["links"] = explode('&nbsp;',$str_links );

	// View data according to array.
	$this->load->view("blog_view", $data);
	}
}