<?php


/**
* 
*/
class Signin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Login');
	}

	public function index()
	{
		$this->load->view('header');  
		$this->load->view('signin');
		$this->load->view('footer');
	}

	public function loghome()  
	{
		$this->load->view('headerinner');  
		$this->load->view('Members');
	}

	public function validate_credentials()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('Pass');

		$result = $this->Login->validate($username,$password);
		if ($result) 
		{
			$this->loghome();
		}
		else
		{
			$this->index();
		}
	}


	public function logout()
	{
		$newdata = array(
		  'user_id'   =>'',
		  'user_email'     => '',
		  'logged_in' => FALSE,
		  );
		 $this->session->unset_userdata($newdata );
		  $this->session->sess_destroy();
		  $this->index();
	}
}