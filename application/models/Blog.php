<?php



/**
* 
*/
class Blog extends CI_Model
{
	public function blogadd($filename,$user_id)
	{

		$data =  array('image' => $filename,
					   'title' => $this->input->post('title'),
					   'descr' => $this->input->post('message'),
					   'userid' => $user_id,
					   'created_date' => date('Y-m-d') 
			);
	//	$this->db->set('created_date', 'NOW()', FALSE);
		$this->db->insert('blog', $data); 
	}


	public function blogdisplay()
	{
		$this->db->select('*');
        $this->db->from('blog');
        $query = $this->db->get();

         return $query->result() ;
	}
}