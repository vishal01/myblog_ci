
	
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Signin</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h4>Get in touch with us by filling <strong>Login Form below</strong></h4>
				<form id="contactform" action="<?=site_url('Signin/validate_credentials')?>" method="post" class="validateform" name="send-contact">
					<div id="sendmessage">
						 Your message has been sent. Thank you!
					</div>
					<div class="row">
							<input type="text" name="username" placeholder="* Enter your username" 
							data-rule="maxlen:4" data-msg="Enter your username" />
							<div class="validation">

							<input type="text" name="Pass" placeholder="* Enter your Password" data-rule="email" data-msg="Enter your Password" />
							<div class="validation">

								<div class="btn-group"> 
									<input type="submit" class="btn btn-primary" name="Register" value="Signin">
									<!-- <button class="btn btn-primary">primary</button>  -->
									 </div>
							
						</div>
						<!-- <div class="col-lg-4 field">
							<input type="text" name="email" placeholder="* Enter your email address" data-rule="email" data-msg="Please enter a valid email" />
							<div class="validation">
							</div>
						</div>
						<div class="col-lg-4 field">
							<input type="text" name="subject" placeholder="Enter your subject" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
							<div class="validation">
							</div>
						</div>
						<div class="col-lg-12 margintop10 field">
							<textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"></textarea>
							<div class="validation">
							</div> -->
							<p>
								<!-- <button class="btn btn-theme margintop10 pull-left" type="submit">Submit message</button> -->
								<span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	</section>
	