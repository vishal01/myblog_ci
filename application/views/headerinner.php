<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Moderna - Bootstrap 3 flat corporate template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://bootstraptaste.com" />
<!-- css -->
<link href="http://blog.com/Assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="http://blog.com/Assets/css/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="http://blog.com/Assets/css/jcarousel.css" rel="stylesheet" />
<link href="http://blog.com/Assets/css/flexslider.css" rel="stylesheet" />
<link href="http://blog.com/Assets/css/style.css" rel="stylesheet" />


<!-- Theme skin -->
<link href="http://blog.com/Assets/skins/default.css" rel="stylesheet" />

<script src="http://blog.com/Assets/js/site.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">
	<!-- start header -->
	<header>
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><span>M</span>oderna</a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Authentication <b class=" icon-angle-down"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php base_url(); ?>/index.php/Register">Register</a></li>
                                <li><a href="<?php base_url(); ?>/index.php/Login">Login</a></li>
								
                            </ul>
                        </li>
                        <li><a href="<?php base_url(); ?>/index.php/Blogview">Portfolio</a></li>
                        <li><a href="<?php base_url(); ?>/index.php/Blog">Blog</a></li>
                        <li><?php echo anchor('Signin/logout', 'Logout'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->