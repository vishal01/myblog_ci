<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 
<script type="text/javascript">
function sendData()
{
 
    var data = new FormData($('#contactform')[0]);
 
 
     $.ajax({
               type:"POST",
               url:"<?php echo site_url('uploadController/do_upload');?>",
               data:data,
               mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false,
               success:function(data)
              {
                        console.log(data);
 
               }
       });
 
}
 
</script>
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Blog</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h4>Post<strong>Your Blog below</strong></h4>
				<?php //echo form_open_multipart('uploadController/do_upload');?>
<?php //echo "<input type='file' name='userfile' size='20' />"; ?>
<?php //echo "<input type='submit' name='submit' value='upload' /> ";?>
<?php //echo "</form>"?>
				<?php //$attributes = array('class' => 'email', 'id' => 'contactform'); ?>
				<?php //echo form_open_multipart('uploadController/do_upload', $attributes);?>
<form id="contactform" action="" method="post" class="validateform" name="send-contact">
        <fieldset>
        	<div class="row">
							<input type="text" name="title" id="title" placeholder="* Enter your Title" 
							data-rule="maxlen:12" data-msg="Please enter at least 12 chars" />
							<div class="validation">

							<textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." 
							data-rule="required" data-msg="Please write something"></textarea>
							<div class="validation">



							</div>
            

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <input type="file" name="userfile" id="userfiles" size="20" />
                        <span class="text-danger"><?php if (isset($error)) { echo $error; } ?></span>
                    </div>
                </div>
            </div>

             

            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" id="submitbtn" value="Upload File" class="btn btn-primary" onclick = "return sendData()"/>
                    </div>
                </div>
            </div>
        </fieldset>
        
        <?php echo form_close(); ?>
				
			</div>
		</div>
	</div>
	</section>

	