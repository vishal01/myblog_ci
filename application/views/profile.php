
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">Home</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<form id="contactform" action="" method="post" class="validateform" name="send-contact">
					<div id="sendmessage">
						 Your message has been sent. Thank you!
					</div>
					<?php echo $user['name'];  ?>
					<div class="row">
							<input type="text" name="fullname" placeholder="* Enter your full name" 
							data-rule="maxlen:4" data-msg="Please enter at least 4 chars" value="<?php  ?>" />
							<div class="validation">

							<input type="text" name="email" placeholder="* Enter your email address" data-rule="email" data-msg="Please enter a valid email" />
							<div class="validation">

							<input type="text" name="phone" placeholder="* Enter your Phone number" data-rule="email" data-msg="Enter your Phone number" />
							<div class="validation">

							<input type="text" name="addr" placeholder="* Enter your Address" data-rule="email" data-msg="Enter your Address" />
							<div class="validation">

							<input type="text" name="username" placeholder="* Enter your username" 
							data-rule="maxlen:4" data-msg="Enter your username" />
							<div class="validation">

							<input type="text" name="Pass" placeholder="* Enter your Password" data-rule="email" data-msg="Enter your Password" />
							<div class="validation">

								<div class="btn-group"> 
									<input type="submit" class="btn btn-primary" name="Register" value="Update">
									<!-- <button class="btn btn-primary">primary</button>  -->
									 </div>
							
						</div>
						
							<p>
								<!-- <button class="btn btn-theme margintop10 pull-left" type="submit">Submit message</button> -->
								<span class="pull-right margintop20">* Please fill all required form field, thanks!</span>
							</p>
						</div>
					</div>
				</form>
				
			</div>
		</div>
	</div>
	</section>
	<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Get in touch with us</h5>
					<address>
					<strong>Moderna company Inc</strong><br>
					 Modernbuilding suite V124, AB 01<br>
					 Someplace 16425 Earth </address>
					<p>
						<i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
						<i class="icon-envelope-alt"></i> email@domainname.com
					</p>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Pages</h5>
					<ul class="link-list">
						<li><a href="#">Press release</a></li>
						<li><a href="#">Terms and conditions</a></li>
						<li><a href="#">Privacy policy</a></li>
						<li><a href="#">Career center</a></li>
						<li><a href="#">Contact us</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Latest posts</h5>
					<ul class="link-list">
						<li><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></li>
						<li><a href="#">Pellentesque et pulvinar enim. Quisque at tempor ligula</a></li>
						<li><a href="#">Natus error sit voluptatem accusantium doloremque</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="widget">
					<h5 class="widgetheading">Flickr photostream</h5>
					<div class="flickr_badge">
						<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=8&amp;display=random&amp;size=s&amp;layout=x&amp;source=user&amp;user=34178660@N03"></script>
					</div>
					<div class="clear">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>
							<span>&copy; Moderna 2014 All right reserved. By </span><a href="http://bootstraptaste.com" target="_blank">Bootstrap Themes</a>
						</p>
                        <!-- 
                            All links in the footer should remain intact. 
                            Licenseing information is available at: http://bootstraptaste.com/license/
                            You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Moderna
                        -->
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="http://blog.com/Assetsjs/jquery.js"></script>
<script src="http://blog.com/Assetsjs/jquery.easing.1.3.js"></script>
<script src="http://blog.com/Assetsjs/bootstrap.min.js"></script>
<script src="http://blog.com/Assetsjs/jquery.fancybox.pack.js"></script>
<script src="http://blog.com/Assetsjs/jquery.fancybox-media.js"></script>
<script src="http://blog.com/Assetsjs/google-code-prettify/prettify.js"></script>
<script src="http://blog.com/Assetsjs/portfolio/jquery.quicksand.js"></script>
<script src="http://blog.com/Assetsjs/portfolio/setting.js"></script>
<script src="http://blog.com/Assetsjs/jquery.flexslider.js"></script>
<script src="http://blog.com/Assetsjs/animate.js"></script>
<script src="http://blog.com/Assetsjs/custom.js"></script>
<script src="http://blog.com/Assetsjs/validate.js"></script>
</body>
</html>